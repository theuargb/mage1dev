<?php

class E6_T1_Model_Payment extends Mage_Payment_Model_Method_Abstract
{
    protected $_code = 'e6t1';

    function isAvailable($quote = null)
    {
        $supportedShippingMethods = [
            "e6t2_standard"
        ];

        return parent::isAvailable($quote)
            && in_array($quote->getShippingAddress()->getShippingMethod(), $supportedShippingMethods);
    }
}