<?php

class E6_T2_Model_Carrier extends Mage_Shipping_Model_Carrier_Abstract
    implements Mage_Shipping_Model_Carrier_Interface
{
    protected $_code = 'e6t2';

    public function getAllowedMethods(): array
    {
        return [
            'standard' => 'Standard delivery',
        ];
    }

    protected function _getStandardRate(): Mage_Shipping_Model_Rate_Result_Method
    {
        /** @var Mage_Shipping_Model_Rate_Result_Method $rate */
        $rate = Mage::getModel('shipping/rate_result_method');

        $rate->setCarrier($this->_code);
        $rate->setCarrierTitle($this->getConfigData('title'));
        $rate->setMethod('standard');
        $rate->setMethodTitle('Standard delivery');
        $rate->setPrice(1.23);
        $rate->setCost(0);

        return $rate;
    }

    public function collectRates(Mage_Shipping_Model_Rate_Request $request): Mage_Shipping_Model_Rate_Result
    {
        /** @var Mage_Shipping_Model_Rate_Result $result */

        $result = Mage::getModel('shipping/rate_result');

        $result->append($this->_getStandardRate());
        return $result;
    }
}