<?php

class E7_T1_Block_CustomerWidgetName extends Mage_Customer_Block_Widget_Name
{
    /**
     * Define if middlename attribute is required
     *
     * @return bool
     */
    public function isMiddlenameRequired()
    {
        $onepage = Mage::getSingleton('checkout/type_onepage');
        $shippingAddress = $onepage->getQuote()->getShippingAddress();
        $shippingMethod = $shippingAddress->getShippingMethod();
        return $shippingMethod == "flatrate_flatrate" || parent::isMiddlenameRequired();
    }
}