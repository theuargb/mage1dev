<?php

require_once Mage::getModuleDir('controllers', 'Mage_Checkout') . DS . 'OnepageController.php';

class E7_T1_OnepageController extends Mage_Checkout_OnepageController
{
    public function saveShippingAction()
    {
        parent::saveShippingAction();

        $shippingAddress = $this->getOnepage()->getQuote()->getShippingAddress();

        $shippingMethod = $shippingAddress->getShippingMethod();
        $middleName = $shippingAddress->getMiddlename();

        if ($shippingMethod == "flatrate_flatrate" && empty($middleName)) {
            $result = [
                'error' => -1,
                'message' => Mage::helper('checkout')->__('Middle name is required for flat rate shipping.')
            ];
            $this->_prepareDataJSON($result);
        }
    }

    /**
     * Shipping method save action
     */
    public function saveShippingMethodAction()
    {
        parent::saveShippingMethodAction();

        $shippingAddress = $this->getOnepage()->getQuote()->getShippingAddress();

        $shippingMethod = $shippingAddress->getShippingMethod();
        $middleName = $shippingAddress->getMiddlename();

        if ($shippingMethod == "flatrate_flatrate" && empty($middleName)) {
            $result['goto_section'] = 'shipping';
            $result['update_section'] = null;
            $this->_prepareDataJSON($result);
        }
    }
}